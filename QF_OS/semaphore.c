#include "semaphore.h"

extern OS_TCB *g_OSTcbCurrentPt;
extern T_LIST g_OSTaskReadyTable[config_SYS_MAX_TASK_NUM];

//T_TCB g_waiteForSemaphore[config_SYS_MAX_SEM_NUM];

int sem_init(sem_t *sem, int pshared, unsigned int value)
{
    if(sem != NULL)
    {
        sem->count = value;
        sem->maxCount = value;
        ListInit(&(sem->holder));
        return OK;
    }
    return ERROR;
}

int sem_post(sem_t *sem)
{
    int needScheduler = 0;
    OS_TCB *tcbP = NULL;
    if(sem == NULL)
    {
        return ERROR;
    }
    
    OSSchedulerSuspend();
    if( sem->count == 0 )
    {
        /*将holder里面的任务转移到就绪列表*/
        while(1)
        {
            if(ListLength(&sem->holder))
            {
                tcbP = (OS_TCB*)ListCurrent(&sem->holder);
                OSSetPrio(tcbP->prio);
                ListNodeMove(&g_OSTaskReadyTable[tcbP->prio], (T_Node*)tcbP, 0);
                needScheduler = 1;
            }
            else
            {
                break;
            }
        }
    }
    if(sem->count<sem->maxCount)
    {
        ++sem->count;
    }
    OSSchedulerResume();
    if(needScheduler)
    {
         OSSwTask();
    }
    return OK;
}

int sem_wait(sem_t *sem)
{
    int needScheduler = 0;
    if(sem == NULL)
    {
        return ERROR;
    }
    
    while(1)
    {
        if(needScheduler)
        {
            OSSwTask();
        }
        OSSchedulerSuspend();
        if( sem->count == 0 )
        {
            /*将当前任务从就绪列表移到sem Holder里*/
            ListNodeMove(&sem->holder, (T_Node*)g_OSTcbCurrentPt, 0);
            OSClearPrio(g_OSTcbCurrentPt->prio);
            needScheduler = 1;
        }
        else
        {
            --sem->count;
            OSSchedulerResume();
            break;
        }
        OSSchedulerResume();
    }
    return OK;
}

int sem_trywait(sem_t *sem)
{
    int ret = ERROR;
    if(sem == NULL)
    {
        return ERROR;
    }
    
    OSSchedulerSuspend();
    if(sem->count)
    {
        --sem->count;
        ret = OK;
    }
    OSSchedulerResume();
    return ret;
}

int sem_getvalue(sem_t *sem,int * sval)
{
    return (*sval = sem->count);
}

int sem_destroy(sem_t *sem)
{
    
    return ERROR;
}
