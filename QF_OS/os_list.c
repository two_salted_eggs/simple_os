#include "os_include.h"

void ListInit(T_LIST *list)
{
    if(list != NULL)
    {
        list->next = NULL;
        list->pre = NULL;
        list->slider = NULL;
        list->length = 0;
    }
}

OS_WORD ListLength(T_LIST *list)
{
    if(list != NULL)
    {
        return list->length;
    }
    else
    {
        return 0;
    }
}

OS_WORD ListInsert(T_LIST *list, T_Node* node, int pos) /*O(1)*/
{
    OS_WORD i;
    T_Node* tempNode = NULL;
    OS_WORD ret = list && node && (node->owner!=list);
    if( ret )
    {
        node->owner = list; /*标记此节点所属的链表*/
        /*链表为空*/
        if(list->length == 0)
        {
            list->next = node;
            list->pre  = node;
            list->slider = node;
            node->next = node;
            node->pre  = node;
        }
        else /*链表不为空*/
        {
            
            if(pos == 0) /*插在头部*/
            {
                tempNode = list->next;
                node->next = tempNode;
                node->pre  = list->pre;
                tempNode->pre = node;
                
                list->pre->next = node;
                list->next = node;
            }
            else if(pos >= list->length) /*插在尾部*/
            {
                node->pre  = list->pre;
                node->next = list->next;
                list->pre->next = node;
                list->pre  = node;
            }
            else /*插在普通位置, 插在tempNode的后边*/
            {
                if(pos <= (list->length>>1)) /*插在前半部分*/
                {
                    tempNode = list->next;
                    for(i=1; i<pos; i++)
                    {
                        tempNode = tempNode->next;
                    }
                }
                else /*插在后半部分*/
                {
                    tempNode = list->pre;
                    for(i=list->length; i>pos; i--)
                    {
                        tempNode = tempNode->pre;
                    }
                }
                
                node->next = tempNode->next;
                node->pre  = tempNode;
                tempNode->next = node;
                node->next->pre = node;
            }
        }
        list->length++;
        
    }
    return ret;
}

OS_WORD ListInsertMax(T_LIST *list, T_Node* node) /*O(n/2)*/
{
    OS_WORD i;
    T_Node* tempNodeNext = NULL;
    T_Node* tempNodePre  = NULL;
    T_Node* tempNode     = NULL;
    int ret = list && node && (node->owner!=list);
    if(ret)
    {
        node->owner = list;
        /*链表为空*/
        if(list->length == 0)
        {
            list->next = node;
            list->pre  = node;
            list->slider = node;
            node->next = node;
            node->pre  = node;
        }
        else /*链表不为空*/
        {
            if(node->data > list->pre->data) /*插在尾部*/
            {
                node->pre  = list->pre;
                node->next = list->next;
                list->pre->next = node;
                list->pre  = node;
            }
            else if(node->data <= list->next->data) /*插在头部*/
            {
                node->next       = list->next;
                node->pre        = list->pre;
                list->next->pre  = node;
                list->next = node;
            }
            else
			{
                /*采用双向查找， 插在tempNode后面*/
                tempNodeNext = list->next; /*指向第一个节点*/
                tempNodePre  = list->pre;  /*指向最后一个节点*/
                for(i=1; i<(list->length); i++)
                {
                    if(node->data <= tempNodeNext->next->data)
                    {
                        tempNode = tempNodeNext;
                    	break;
					}
					else if( node->data > tempNodePre->pre->data)
                    {
                        tempNode = tempNodePre->pre;
                        break;
                    }
                    tempNodeNext = tempNodeNext->next;
                    tempNodePre  = tempNodePre->pre;
                }
                node->next = tempNode->next;
                node->pre  = tempNode;
                tempNode->next = node;
                node->next->pre = node;
            }
        }
        list->length++;
    }
    return ret;
}

T_Node* ListGet(T_LIST *list, int pos) /*O(n/2)*/
{
    OS_WORD i;
    T_Node* retNode = NULL;
    if(list != NULL)
    {
        if(list->length > 0) /*链表中有数据*/
        {
            if(pos == 0 )  /*获取头节点数据*/
            {
                retNode = list->next;
            }
            else if(pos >= list->length) /*获取尾节点数据*/
            {
                retNode = list->pre;
            }
            else 
            {
                if(pos <= (list->length>>1))
                {
                    retNode = list->next;
                    for(i=0; i<pos; i++)
                    {
                        retNode = retNode->next;
                    }
                }
                else
                {
                    retNode = list->pre;
                    for(i=list->length-1; i>pos; i--)
                    {
                        retNode = retNode->next;
                    }
                }
                
            }
        }
    }
    return retNode;
}

T_Node* ListNext(T_Node* node) /*O(1)*/
{
    T_Node *retNode = NULL;
    if(node != NULL && node->owner != NULL)
    {
        retNode = node->next;
    }
    return retNode;
}

T_Node* ListCurrent(T_LIST *list) /*O(1)*/
{
    if(list != NULL)
    {
        return list->slider;
    }
    else
    {
        return NULL;
    }
}

T_Node* ListDel(T_LIST *list, OS_WORD pos) /*O(n/2)*/
{
    OS_WORD i;
    T_Node *tempNode = NULL, *retNode = NULL;
    if(list != NULL) /*从0开始删除*/
    {
        if(list->length > 0) /*链表中有数据*/
        {
            if(pos == 0) /*删除头节点*/
            {
                retNode = list->next;
                list->next->pre = list->pre;
                list->next = list->next->next;
                if(retNode == list->slider)
                {
                    list->slider = list->next;
                }
            }
            else if(pos >= list->length) /*删除尾节点*/
            {
                retNode = list->pre;
                list->pre->next = list->next;
                list->pre = list->pre->pre;
                if(retNode == list->slider)
                {
                    list->slider = list->pre;
                }
                
            }
            else /*找到要删除的节点的前一个节点*/
            {
                if(pos <= (list->length>>1))
                {
                    tempNode = list->next;
                    for(i=1; i<pos; i++)
                    {
                        tempNode = tempNode->next;
                    }
                }
                else
                {
                    tempNode = list->pre;
                    for(i=list->length; i>pos; i--)
                    {
                        tempNode = tempNode->next;
                    }
                }
                retNode = tempNode->next;
                tempNode->next = retNode->next;
                retNode->next->pre = tempNode;
                if(retNode == list->slider)
                {
                    list->slider = tempNode->next;
                }
            }
            retNode->owner = NULL;
            list->length--;
        }
    }
    return retNode;
}

T_Node* ListDelNode(T_Node* node) /*O(1)*/
{
    T_Node* tempNode = NULL, *retNode = NULL;
    if( node != NULL && node->owner != NULL )
    {
        T_LIST *list = node->owner;
        if(list->length > 0) /*链表中有数据*/
        {
            if(node == list->slider)
            {
                list->slider = node->next;
            }
            
            if(node == list->next) /*删除头节点*/
            {
                retNode = list->next;
                list->next = list->next->next;
                list->next->pre = list->pre;
                list->pre->next = list->next;
            }
            else if(node == list->pre) /*删除尾节点*/
            {
                retNode = list->pre;
                list->pre = list->pre->pre;
                list->next->pre = list->pre;
                list->pre->next = list->next;
                
            }
            else
            {
                tempNode = node->pre;
                retNode = tempNode->next;
                tempNode->next = retNode->next;
                retNode->next->pre = tempNode;
            }
            retNode->owner = NULL;
            list->length--;
        }
    }
    return retNode;
}

void ListClear(T_LIST *list)
{
	OS_WORD i;
    if(list != NULL)
    {
    	for(i=0; ListLength(list); i++)
    	{
    		ListDel(list,0);
		}
    }
}

void ListNodeMove(T_LIST *desList, T_Node *node, int pos)
{
    if(desList && node)
    {
        ListDelNode(node);
        ListInsert(desList, node, pos);
    }
}
