#ifndef __OS_TASK_H
#define __OS_TASK_H

#include "os_type.h"

#define CPU_EXCEPTSTACK_SIZE    (1)
#define OS_MAX_DELAY            ((OS_WORD)-1)

/*

*/

typedef struct os_tcb
{
    OS_STK          *OSTCBStkPtr; /*当前任务的堆栈*/
    struct os_tcb   *next;        /*指向下一个任务控制块*/
    struct os_tcb   *pre;         /*指向当前任务的前一项任务*/
    void            *owner;       /*节点所属的链表*/
    OS_TICK         nextTime;     /*用于任务延时*/
    OS_TICK         leftTime;     /*用于时间片调度是的时间剩余量*/
    OS_TICK         usedTime;     /*任务所占用的总时间*/
    OS_WORD         prio;         /*优先级*/
    OS_TASK_STATU   statu;        /*任务状态*/
}OS_TCB;


typedef void (*OS_taskFun)(void* param);

OS_TICK OSTickGet(void);
OS_TICK OSTaskTickGet(void);
void OSInit(void);
OS_TASK_ID OSTaskCreate(OS_TCB* aTcb, OS_taskFun aTask, void *aParam, uint16_t aPrio, OS_STK* aStack, uint32_t aStkSize);
void OSSwTask(void);
void OSTaskDelay(OS_WORD aTick);
void OSTick(void);

void OSSetPrio(uint8_t aPrio);
void OSClearPrio(uint8_t aPrio);

void OSSchedulerSuspend(void);
void OSSchedulerResume(void);

#endif /*__OS_TASK_H*/
