#ifndef __OS_CONFIG_H_
#define __OS_CONFIG_H_

/*
编码风格：
枚举定义采用全大写
#define定义采用全大写

全局变量
{
    g_ 命名
    全局变量指针Pt结尾
}
局部变量
{
    指针用p打头
}

函数用大写打头编写

函数参数用a打头
*/

#define config_SYS_TIME_SLICE_SIZE      (2)  /*系统默认的时间片调度的大小*/
#define config_SYS_MAX_TASK_NUM         (10)   /*系统中最大任务数*/
#define config_IDLE_TASK_STK_SIZE       (128) /*空闲任务堆栈大小*/
#define config_TIMER_TASK_STK_SIZE      (128) /*定时器任务堆栈大小*/


#endif /*__OS_CONFIG_H_*/



