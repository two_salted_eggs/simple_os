#ifndef __SEMAPHORE_H_
#define __SEMAPHORE_H_

#include <time.h>
#include "os_list.h"

typedef struct
{
    T_LIST holder; /*信号量持有者*/
    int    maxCount;
    int    count;  /*信号量计数器*/
}sem_t;

/*命名信号量*/
//sem_t *sem_open(const char *name, int oflag, ... /* mode_t mode, unsigned int value */ );
//int sem_close(sem_t *sem);
//int sem_unlink(const char *name);
  
int sem_init(sem_t *sem, int pshared, unsigned int value);
  
int sem_post(sem_t *sem);

int sem_wait(sem_t *sem);

int sem_trywait(sem_t *sem);

//int sem_timedwait(sem_t * sem, const struct timespec *tsptr);

int sem_getvalue(sem_t *sem,int * sval);

int sem_destroy(sem_t *sem);


#endif /*__OS_SEM_H_*/
