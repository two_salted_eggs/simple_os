#ifndef __OS_LIST_H_
#define __OS_LIST_H_

#include "os_include.h"

typedef struct node
{
    void          *pt;
    struct node   *next;        /*指向下一个节点*/
    struct node   *pre;         /*指向当前节点的前节点*/
    void          *owner;
    OS_WORD       data;
}T_Node;

typedef struct
{
    T_Node  *next;    /*队列的头*/
    T_Node  *pre;     /*队列的尾巴*/
    T_Node  *slider;  /*滑块*/
    OS_WORD length;   /*记录队列中的个数*/
}T_LIST;

void ListInit(T_LIST *list);
OS_WORD ListLength(T_LIST *list);
OS_WORD ListInsert(T_LIST *list, T_Node* node, int pos);
OS_WORD ListInsertMax(T_LIST *list, T_Node* node);
T_Node* ListGet(T_LIST *list, int pos);
T_Node* ListNext(T_Node* node);
T_Node* ListDel(T_LIST *list, OS_WORD pos);
T_Node* ListDelNode(T_Node* node);
void ListClear(T_LIST *list);
void ListNodeMove(T_LIST *desList, T_Node *node, int pos);
T_Node* ListCurrent(T_LIST *list);
#define ListInsertEnd(listP, nodeP)   ListInsert(listP, nodeP, (listP)->length)
#define ListInsertFirst(listP, nodeP) ListInsert(listP, nodeP, 0)

#endif /*__OS_LIST_H_*/
