#ifndef __OS_PORT_H_
#define __OS_PORT_H_

#include "os_include.h"

void OS_ENTER_CRITICAL(void);
void OS_EXIT_CRITICAL(void);

void OSCtxSw(void);
void OSStart(void);
void PendSV_Handler(void);

#endif /*__OS_PORT_H_*/


