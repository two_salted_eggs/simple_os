#ifndef __OS_TYPE_H_
#define __OS_TYPE_H_

#include "os_include.h"

typedef enum
{
    OS_TASK_CREATE, /*����״̬*/
    OS_TASK_RUNING, /*����̬*/
    OS_TASK_READY,  /*����̬*/
    OS_TASK_DELAY,  /*��ʱ̬*/
    OS_TASK_PEND    /*����̬*/
}OS_TASK_STATU;

#ifndef TRUE
#define TRUE  (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef OK
#define OK (0)
#endif

#ifndef ERROR
#define ERROR (-1)
#endif

typedef unsigned int OS_STK;
typedef unsigned int OS_TICK;
typedef unsigned int OS_WORD;
typedef unsigned int OS_TASK_ID;

#endif /*__OS_TYPE_H_*/
